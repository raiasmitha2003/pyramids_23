def high_pyramid(num):
    return sum(x * x for x in range(num + 1))

def low_even_pyramid(num):
    if num % 2 == 0 and num > 2:
        return sum(x * x for x in range(2,  num + 1,  2))
    return -1

